import React from 'react';
// import "./base.css";
const AboutUs = () => (
    <section className="text-center section-about">
        <div className="container">
    
             <h1 className="w-75 mx-auto ">About Us</h1>
             <p className="w-75 mx-auto">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate aut at veritatis officia hic earum veniam dolorem inventore architecto consectetur.
               Card Title</p>   
            <div className="row mb-5">
                <div className="col-12 col-md-4">
                    <div className="card pt-3">
                        <div className="card-text">
                            <div className="card-title">
                                <h4>card title</h4>
                            </div>
                            <div className="card-body">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-md-4">
                    <div className="card pt-3">
                        <div className="card-text">
                            <div className="card-title">
                                <h4>card title</h4>
                            </div>
                            <div className="card-body">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-md-4">
                    <div className="card pt-3">
                        <div className="card-text">
                            <div className="card-title">
                                <h4>card title</h4>
                            </div>
                            <div className="card-body">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button className="btn custom-bttn">Click here</button>
        </div>
    </section>
 
    );
    



export default AboutUs;