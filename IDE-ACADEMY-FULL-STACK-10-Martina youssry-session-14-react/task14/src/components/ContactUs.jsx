import React from 'react';
// import "./base.css"
const ContactUs = () => (
    <section className="contact-section text-center">
        <div className="container">
            <h1>Contact Us</h1>
            <p>For any inquiries message us</p>
            <form>
                <div class="row form-row mb-5">
                    <div class="col-6 p-4 d-flex flex-column ">
                        <input type="text" id="name" class="form-control mb-5" placeholder="Enter Your Name"/>
                        <input type="text" id="email" class="form-control mb-5" placeholder="Enter Your Email"/>
                        <input type="text" id="phone" class="form-control" placeholder="Enter Your Phone Number"/>
                    </div>
                    <div class="col-6 p-4 d-flex flex-stretch">
                        <textarea id="message" class="form-control" placeholder="Enter Your Message"></textarea>
                    </div>
                </div>
                <button class="custom-bttn btn-light">Click Here</button>
            </form>
        </div>

    </section>
);

export default ContactUs;