import React from 'react';
// import "./base.css";

const Header = () => (
<header className ="header-class">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">

    <div className="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="#"> brand</a>
        <ul class="navbar-nav ml-auto">
        <li class="nav-item ">
            <a class="nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#">about</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#">contact</a>
        </li>
        </ul>
   
  </div>
    </div>
  
</nav>
</header>    
);

export default Header;