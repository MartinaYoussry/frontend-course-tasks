import React from 'react';
// import "./base.css"

const Footer = () => (
    <footer>
        <div class="container">
            <div class="row text-center d-flex">
                <div class="col-3">
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                </div>
                <div class="col-6">
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                </div>
                <div class="col-3">
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                    <a class="d-block my-auto" href="#">Link</a>
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;