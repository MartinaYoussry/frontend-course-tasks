import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import ContactUs from "./components/ContactUs";
import AboutUs from "./components/AboutUs";
import 'bootstrap/dist/css/bootstrap.min.css'
import "./App.css";


const App = () => (
    <div >
        <Header/>
        <AboutUs/>
        <ContactUs/>
        <Footer/>
    </div>
);

export default App;